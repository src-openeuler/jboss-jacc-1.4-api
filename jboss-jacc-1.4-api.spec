Name:           jboss-jacc-1.4-api
Version:        1.0.2
Release:        16
Summary:        JBoss JACC 1.4 API
License:        CDDL-1.0 or GPL-2.0-with-classpath-exception 
URL:            http://www.jboss.org
# git clone git://github.com/jboss/jboss-jacc-api_spec.git
# cd jboss-jacc-api_spec/
# git archive --format=tar --prefix=jboss-jacc-1.4-api/ jboss-jacc-api_1.4_spec-1.0.2.Final | xz > jboss-jacc-1.4-api-1.0.2.Final.tar.xz
Source0:        jboss-jacc-1.4-api-1.0.2.Final.tar.xz
BuildRequires:  maven-local mvn(junit:junit) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-source-plugin) mvn(org.jboss:jboss-parent:pom:)
BuildRequires:  mvn(org.jboss.spec.javax.servlet:jboss-servlet-api_3.0_spec)
BuildArch:      noarch

%description
JBoss Java Authorization Contract for Containers 1.4 API.

%package javadoc
Summary: Javadocs for jboss-jacc-1.4-api

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n jboss-jacc-1.4-api
%mvn_file : jboss-jacc-1.4-api/jboss-jacc-1.4-api jboss-jacc-1.4-api

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc README LICENSE

%files javadoc -f .mfiles-javadoc
%doc LICENSE README

%changelog
* Wed May 11 2022 liyanan <liyanan32@h-partners.com> - 1.0.2-16
- License compliance rectification

* Thu Dec 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.2-15
- Package init
